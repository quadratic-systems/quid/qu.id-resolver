import { readContract, createConfig, http } from '@wagmi/core';
import { createPublicClient } from 'viem';
import { mainnet, arbitrum, sepolia, anvil } from 'viem/chains';
import registrarAbi from '../abis/registrar.js';

const chains = [mainnet, arbitrum, sepolia, anvil];

const config = createConfig({
  chains,
  client ({ chain }) {
    return createPublicClient({
      chain,
      transport: http(process.env.TRANSPORT_URL)
    });
  }
});

export const getPrefix = async () => {
  return await readContract(
    config, {
      abi: registrarAbi,
      address: process.env.REGISTRAR_CONTRACT,
      functionName: 'PREFIX'
    });
};
