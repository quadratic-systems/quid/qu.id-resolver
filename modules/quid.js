import { readContract, createConfig, http } from '@wagmi/core';
import { createPublicClient } from 'viem';
import { mainnet, arbitrum, sepolia, anvil } from 'viem/chains';
import quidAbi from '../abis/quid.js';

const chains = [mainnet, arbitrum, sepolia, anvil];

const config = createConfig({
  chains,
  client ({ chain }) {
    return createPublicClient({
      chain,
      transport: http(process.env.TRANSPORT_URL)
    });
  }
});

export const getTokenURI = async (tokenId) => {
  return await readContract(
    config, {
      abi: quidAbi,
      address: process.env.QUID_CONTRACT,
      functionName: 'tokenURI',
      args: [tokenId]
    });
};
