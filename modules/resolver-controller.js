import { getTokenURI } from './quid.js';
import { getPrefix } from './registrar.js';

const PREFIX = await getPrefix();

export const getRoot = async (req, res) => {
  try {
    if (req.accepts('text/html')) {
      res.sendStatus(404);
    } else if (req.accepts('application/json')) {
      res.json({
        prefix: '',
        tokenId: '',
        tokenURI: ''
      });
    } else res.sendStatus('406');
  } catch (e) {
    res.status(e.status || 500).json({ error: e.message });
    console.error(e);
  }
};

export const getRootWithParams = async (req, res) => {
  try {
    if (req.params.prefix !== PREFIX.toString()) throw Error('Invalid prefix');
    const tokenURI = await getTokenURI(req.params.tokenId);
    const suffix = req.params.suffix;
    if (req.accepts('text/html')) {
      (suffix) ? res.redirect(`${tokenURI}/${suffix}`) : res.redirect(tokenURI);
    } else if (req.accepts('application/json')) {
      res.json({
        prefix: req.params.prefix,
        tokenId: req.params.tokenId,
        tokenURI,
        suffix
      });
    } else res.sendStatus('406');
  } catch (e) {
    res.status(e.status || 500).json({ error: e.message });
    console.error(e);
  }
};
