import express from 'express';
import cors from 'cors';
import { getRoot, getRootWithParams } from './modules/resolver-controller.js';

// Create the app
const app = express();
app.use(express.json());
app.use(cors());

// Define the routes
app.get('/', getRoot);
app.get([
  '/:prefix.:tokenId',
  '/:prefix.:tokenId/:suffix(*)'
], getRootWithParams);

// Start the app
app.listen(process.env.HTTP_PORT);
