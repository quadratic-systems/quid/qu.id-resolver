# qu.id-resolver

Resolves a Quadratic Identifier (QuId) to a URI using the Qu.Id contract's tokenURI function.

## Setup

1. `npm i`
2. Create the `.env` file as below and fix the `TRANSPORT_URL` variable

```
HTTP_PORT=8000
REGISTRAR_CONTRACT=0xE0B021272b4C2688B210a0A13D263d2af843290F
QUID_CONTRACT=0x2264aa0e3E1E34a640e45782CBA17f2bfD7FA444
TRANSPORT_URL=https://eth-sepolia.g.alchemy.com/v2/XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

## Testing

### Start the server
```
npm run dev
```

### Test with Curl

The server will respond with json or text/plain, depending on the header included in the request.

#### application/json
```
curl -sH "Accept: application/json" http://localhost:8000/64.1 | jq .
```
```
{
  "prefix": "64",
  "tokenId": "1",
  "tokenURI": "https://x.com/teppentom"
}
```

#### text/plain
```
curl -si http://localhost:8000/64.1
```
```
HTTP/1.1 302 Found
X-Powered-By: Express
Access-Control-Allow-Origin: *
Location: https://x.com/teppentom
Vary: Accept
Content-Type: text/plain; charset=utf-8
Content-Length: 45
Date: Thu, 24 Oct 2024 02:31:05 GMT
Connection: keep-alive
Keep-Alive: timeout=5

Found. Redirecting to https://x.com/teppentom
```

## Docker
You can also run the resolver in Docker and then perform the same testing with curl.

### Build
```
docker build --tag quid-resolver ./
```

### Run
```
docker run -d -p 8000:8000 quid-resolver
```

### Debug
```
docker run -it quid-resolver bash
```
